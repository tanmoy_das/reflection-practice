package net.therap.reflection.domain;


import net.therap.reflection.controller.StudentValidator;

/**
 * @author tanmoy.das
 * @since 3/4/20
 */
public class Student {

    private String session;
    private String registrationNumber;

    @Size(min = 3, max = 100, validatorClass = StudentValidator.class, message = "Name length must be between {min} and {max}")
    private String name;

    @Size(min = 3, max = 100, validatorClass = StudentValidator.class)
    private int age;

    public Student(String session, String registrationNumber, String name, int age) {
        this.session = session;
        this.registrationNumber = registrationNumber;
        this.name = name;
        this.age = age;
    }

    public Student() {
    }

    public String getSession() {
        return session;
    }

    public void setSession(String session) {
        this.session = session;
    }

    public String getRegistrationNumber() {
        return registrationNumber;
    }

    public void setRegistrationNumber(String registrationNumber) {
        this.registrationNumber = registrationNumber;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }
}
