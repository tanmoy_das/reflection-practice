package net.therap.reflection.domain;

import net.therap.reflection.controller.DefaultValidator;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * @author tanmoy.das
 * @since 3/4/20
 */

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
public @interface Size {
    int max() default 1000000;

    int min() default 0;

    String message() default "{field} value must be between {min} and {max}";

    Class validatorClass() default DefaultValidator.class;
}
