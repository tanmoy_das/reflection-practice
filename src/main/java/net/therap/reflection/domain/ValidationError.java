package net.therap.reflection.domain;

/**
 * @author tanmoy.das
 * @since 3/4/20
 */
public class ValidationError extends Exception {
    public ValidationError() {
    }

    public ValidationError(String message) {
        super(message);
    }
}
