package net.therap.reflection;


import net.therap.reflection.controller.AnnotatedValidator;
import net.therap.reflection.controller.ValidatorClassException;
import net.therap.reflection.domain.Student;
import net.therap.reflection.domain.ValidationError;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;

/**
 * @author tanmoy.das
 * @since 3/4/20
 */
public class Main {

    public static void main(String[] args) throws IllegalAccessException, ValidatorClassException, InstantiationException {
//        Person p = new Person("Abcde Fghijk", 5);

        Student student = new Student("2015-2016", "2015331025", "TKD", 2);
        Student student2 = new Student("2015-2016", "2015331025", "", 25);
        Student student3 = new Student("2015-2016", "2015331025", "TKD", 25);

        Collection<Student> students = new HashSet<>();
        students.add(student);
        students.add(student2);
        students.add(student3);

        List<ValidationError> errors = new ArrayList<>();

        AnnotatedValidator.validate(students, errors);
        AnnotatedValidator.print(errors);
    }
}
