package net.therap.reflection.controller;

import net.therap.reflection.domain.Size;
import net.therap.reflection.domain.Student;

import java.lang.reflect.Field;

/**
 * @author tanmoy.das
 * @since 3/4/20
 */
public class StudentValidator implements Validator<Student> {

    @Override
    public boolean isValid(Student student, Field field) throws IllegalAccessException {
        String fieldName = field.getName();

        if(fieldName.equals("age")) {
            Size sizeAnnotation = field.getAnnotation(Size.class);
            Integer age = field.getInt(student);
            return age>=sizeAnnotation.min() && age<=sizeAnnotation.max();
        }

        if(fieldName.equals("name")) {
            Size sizeAnnotation = field.getAnnotation(Size.class);
            Integer nameLength = ((String) field.get(student)).length();
            return nameLength>=sizeAnnotation.min() && nameLength<=sizeAnnotation.max();
        }

        return false;
    }
}
