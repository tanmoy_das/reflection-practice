package net.therap.reflection.controller;

import net.therap.reflection.domain.Size;

import java.lang.reflect.Field;

/**
 * @author tanmoy.das
 * @since 3/4/20
 */
public class DefaultValidator<T> implements Validator<T> {

    @Override
    public boolean isValid(T obj, Field field) {
        field.setAccessible(true);

        Size sizeAnnotation = field.getAnnotation(Size.class);
        if(sizeAnnotation == null) {
            return true;
        }

        if(Integer.class.isAssignableFrom(field.getType())) {
            try {
                Integer val = (Integer) field.get(obj);
                return (val.compareTo(sizeAnnotation.min()) >= 0) && (val.compareTo(sizeAnnotation.max()) <= 0);
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
        }

        if(String.class.isAssignableFrom(field.getType())) {
            try {
                int val = ((String) field.get(obj)).length();
                return val>=sizeAnnotation.min() && val<=sizeAnnotation.max();
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
        }

        return false;
    }
}
