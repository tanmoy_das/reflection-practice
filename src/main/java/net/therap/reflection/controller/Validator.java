package net.therap.reflection.controller;

import java.lang.reflect.Field;

/**
 * @author tanmoy.das
 * @since 3/4/20
 */
public interface Validator <T> {
    public boolean isValid(T obj, Field field) throws IllegalAccessException;
}
