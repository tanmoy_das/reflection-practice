package net.therap.reflection.controller;

import net.therap.reflection.domain.Person;
import net.therap.reflection.domain.Size;
import net.therap.reflection.domain.ValidationError;

import java.lang.reflect.Field;
import java.util.Collection;
import java.util.List;

/**
 * @author tanmoy.das
 * @since 3/4/20
 */

public class AnnotatedValidator {

    public static <T> void validate(Collection<T> objects, List<? super ValidationError> errors) throws ValidatorClassException, InstantiationException, IllegalAccessException {
        for(T object : objects) {
            validate(object, errors);
        }
    }

    public static <T> void validate(T object, List<? super ValidationError> errors) throws ValidatorClassException, IllegalAccessException, InstantiationException {
        Class objClass = object.getClass();

        String message = "";

        for (Field field : objClass.getDeclaredFields()) {
            field.setAccessible(true);
            Size sizeAnnotation = field.getAnnotation(Size.class);

            if(sizeAnnotation==null) {
                continue;
            }

            Class validatorClass = sizeAnnotation.validatorClass();

            if (Validator.class.isAssignableFrom(validatorClass)) {
                Validator<T> validator = (Validator<T>) validatorClass.newInstance();

                if (!validator.isValid(object, field)) {
                    if (!message.isEmpty()) {
                        message += ", ";
                    }

                    String msg = sizeAnnotation.message();

                    msg = msg.replace("{field}", field.getName());
                    msg = msg.replace("{min}", String.valueOf(sizeAnnotation.min()));
                    msg = msg.replace("{max}", String.valueOf(sizeAnnotation.max()));

                    message += msg;
                }
            } else {
                throw new ValidatorClassException();
            }
        }

        if(!message.isEmpty()) {
            errors.add(new ValidationError(message));
        }

    }

    public static void print(List<ValidationError> errors) {
        for (ValidationError validationError : errors) {
            System.out.println(validationError);
        }
    }
}
