package net.therap.reflection.controller;

/**
 * @author tanmoy.das
 * @since 3/4/20
 */
public class ValidatorClassException extends Exception {
    public ValidatorClassException() {
        super("Class must implement the Validator interface");
    }
}
